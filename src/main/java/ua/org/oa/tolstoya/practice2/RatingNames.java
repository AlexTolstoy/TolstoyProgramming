package ua.org.oa.tolstoya.practice2;

/**
 * Created by Alex on 08.11.2016.
 */
public class RatingNames {
    private int position;
    private String maleName;
    private String femaleName;

    public RatingNames(int position, String maleName, String femaleName) {
        setPosition(position);
        setMaleName(maleName);
        setFemaleName(femaleName);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getMaleName() {
        return maleName;
    }

    public void setMaleName(String maleName) {
        this.maleName = maleName;
    }

    public String getFemaleName() {
        return femaleName;
    }

    public void setFemaleName(String femaleName) {
        this.femaleName = femaleName;
    }

    @Override
    public String toString() {
        return "RatingNames{" +
                "position=" + position +
                ", maleName='" + maleName + '\'' +
                ", femaleName='" + femaleName + '\'' +
                '}';
    }
}
