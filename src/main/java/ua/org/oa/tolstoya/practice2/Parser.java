package ua.org.oa.tolstoya.practice2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alex on 08.11.2016.
 */
public class Parser {
    public static String parseString(String path, String charset) {
        StringBuilder sb = new StringBuilder();
        String s = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), charset))) {
            while ((s = br.readLine()) != null) {
                sb.append(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static List<RatingNames> createRatingNames() {
        String s = Parser.parseString("baby2008.html", "utf-8");
        List<RatingNames> topNames = new ArrayList<>();
        Pattern pattern = Pattern.compile("<tr align=\"right\"><td>(?<position>\\d+)</td><td>(?<male>\\w+)</td><td>(?<female>\\w+)</td></tr>");
        Matcher matcher = pattern.matcher(s);
        while (matcher.find()) {
            topNames.add(new RatingNames(Integer.parseInt(matcher.group("position")), matcher.group("male"), matcher.group("female")));
        }
        return topNames;
    }
}
