package ua.org.oa.tolstoya.practice3.entity;

/**
 * Created by Alex on 15.11.2016.
 */
public interface GenericStorage<K, V> {
    K add(V value);
    V get(K id);
    V delete(K id);
    boolean upDate(K id, V value);
}
