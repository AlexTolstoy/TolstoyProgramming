package ua.org.oa.tolstoya.practice3.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alex on 15.11.2016.
 */
public abstract class Entity<T> {
    @Setter @Getter
    private T id;
}
