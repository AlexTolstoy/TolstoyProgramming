package ua.org.oa.tolstoya.practice3.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alex on 15.11.2016.
 */
public class User {
    @Getter @Setter
    private String login;
    @Getter @Setter
    private String password;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private String sername;
}
