package ua.org.oa.tolstoya.homework3;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;

/**
 * Created by Alex on 25.11.2016.
 */
public class GenericStorage<T> {
    @Getter
    @Setter
    private T[] storage;
    @Getter
    @Setter
    private int size;

    public GenericStorage() {
        storage = (T[]) new Object[10];
    }

    public GenericStorage(int size) {
        storage = (T[]) new Object[size];
    }

    public boolean add(T obj) {
        if (size < storage.length) {
            storage[size++] = obj;
            return true;
        }
        return false;
    }

    public T get(int index) {
        if (index >= 0 && index < storage.length) {
            return storage[index];
        } else {
            System.out.println("Wrong index, input index from 0 to " + storage.length);
        }
        return null;
    }

    public T[] getAll() {
        return storage;
    }

    public boolean update(int index, T obj) {
        if (index >= 0 && index < storage.length) {
            if (storage[index] != null) {
                storage[index] = obj;
                return true;
            } else {
                System.out.println("Element of index = null\nInput index from 0 to " + (size - 1));
            }
        } else {
            System.out.println("Wrong index, input index from 0 to " + (storage.length - 1));
        }
        return false;
    }

    public boolean delete(int index) {
        if (index >= 0 && index < storage.length) {
            if (storage[index] != null) {
                System.arraycopy(storage, index + 1, storage, index, storage.length - index - 1);
                storage[storage.length - 1] = null;
                size--;
                return true;
            } else {
                System.out.println("Element of index = null\nInput index from 0 to " + (size - 1));
            }
        } else {
            System.out.println("Wrong index, input index from 0 to " + (storage.length - 1));
        }
        return false;
    }

    public boolean delete(T obj) {
        for (int i = 0; i < storage.length; i++) {
            if (obj.equals(storage[i])) {
                System.arraycopy(storage, i + 1, storage, i, storage.length - i - 1);
                storage[storage.length - 1] = null;
                size--;
                return true;
            }
        }
        System.out.println("The element not found");
        return false;
    }

    @Override
    public String toString() {
        return "GenericStorage{" +
                "storage=" + Arrays.toString(storage) +
                '}';
    }
}
