package ua.org.oa.tolstoya.homework3;

import java.util.Arrays;

/**
 * Created by Alex on 25.11.2016.
 */
public class App {
    public static void main(String[] args) {
        GenericStorage<String> genericStorage = new GenericStorage<>(7);
        genericStorage.add("Audi");
        genericStorage.add("BMW");
        genericStorage.add("Mercedes");
        genericStorage.add("Volkswagen");
        genericStorage.add("Skoda");

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Метод T[] getAll(), который вернет массив элементов.");
        System.out.println(Arrays.toString(genericStorage.getAll()));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Метод T get(int index), который возвратит элемент по индексу в масиве.");
        System.out.println(genericStorage.get(3));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Метод update(int index, T obj), который подменит объект по заданной позиции только, " +
                "если на этой позиции уже есть элемент.");
        System.out.println(genericStorage.update(3, "Porsche"));
        System.out.println(Arrays.toString(genericStorage.getAll()));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Meтод delete(int index), который удалит элемент по индексу и захлопнет пустую ячейку в массиве, " +
                "если на этой позиции уже есть элемент.");
        System.out.println(genericStorage.delete(3));
        System.out.println(Arrays.toString(genericStorage.getAll()));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Метод delete(T obj), который удалит заданный объект из массива.");
        System.out.println(genericStorage.delete("BMW"));
        System.out.println(Arrays.toString(genericStorage.getAll()));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Метод который возвратит размер ЗАПОЛНЕНОГО массива.");
        System.out.println(genericStorage.getSize());
    }
}
