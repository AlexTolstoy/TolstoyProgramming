package ua.org.oa.tolstoya.homework6;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Alex on 24.12.2016.
 */
public class FileUtils {
    public static void readConsole() {
        String[] s;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            while (true) {
                System.out.println("Введите имя файла и команду: add, delete, rename (+ новое имя файла), list");
                s = br.readLine().split(" ");
                switch (s[1]) {
                    case "add":
                        addFile(s[0]);
                        break;
                    case "delete":
                        deleteFile(s[0]);
                        break;
                    case "rename":
                        renameFile(s[0], s[2]);
                        break;
                    case "list":
                        listFile(s[0]);
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void addFile(String path) {
        File file = new File(path);
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteFile(String s) {
        File file = new File(s);
        file.delete();
    }

    public static void renameFile(String s, String r) {
        File file = new File(s);
        file.renameTo(new File(r));
    }

    public static void listFile(String s) {
        File file = new File(s);
        if (file.isDirectory()) {
            for (File file1 : file.listFiles()) {
                System.out.println(file1);
            }
        }
    }
}
