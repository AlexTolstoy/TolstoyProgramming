package ua.org.oa.tolstoya.homework6;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by Alex on 24.12.2016.
 */
@Getter
@Setter
@ToString
public class Book implements Serializable{
    private String author;
    private String title;
    private int year;

    public Book(String[] s) {
        setAuthor(s[0]);
        setTitle(s[1]);
        setYear(Integer.parseInt(s[2]));
    }

    public Book(String author, String title, int year) {
        setAuthor(author);
        setTitle(title);
        setYear(year);
    }
}
