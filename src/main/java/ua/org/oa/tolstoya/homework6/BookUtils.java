package ua.org.oa.tolstoya.homework6;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 24.12.2016.
 */
public class BookUtils {
    public static List<Book> inputFile(String path) {
        List<Book> books = new ArrayList<>();
        String s;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            while ((s = br.readLine()) != null) {
                books.add(new Book(s.split(";")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return books;
    }

    public static void serialization(Book book, String path) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))){
            oos.writeObject(book);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
