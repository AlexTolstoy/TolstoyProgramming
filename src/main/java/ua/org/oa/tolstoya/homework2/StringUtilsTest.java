package ua.org.oa.tolstoya.homework2;

/**
 * Created by Alex on 12.11.2016.
 */
public class StringUtilsTest {
    public static void main(String[] args) {
        StringUtils.backSymbols("Hello World!");
        System.out.println(StringUtils.stringPalindrom("А роза упала на лапу Азора"));
        StringUtils.stringLength("Hello!");
        System.out.println(StringUtils.replaceWord("Hello, This Wonderful World!"));
        System.out.println(StringUtils.replaceWordInSentence("Hello, This Wonderful World! This number two."));
        StringUtils.abcContains("abcabcabc");
        StringUtils.dateContains("01.12.2016");
        StringUtils.adressContains("61033, г. Харьков, ул. Шевченко, д. 100, кв. 100");
        StringUtils.containsPhoneNumber("61033, г. Харьков+380(67)555-77-77,+380(67)666-77-77 ул. Шевченко, +380(67)777-77-77 д. 100, кв. 100");
        StringUtils.markDown("MarkDown");
    }
}
