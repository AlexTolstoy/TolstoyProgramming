package ua.org.oa.tolstoya.homework2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alex on 12.11.2016.
 */
public class StringUtils {
    static void backSymbols(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = s.length() - 1; i >= 0; i--) {
            sb.append(s.charAt(i));
        }
        System.out.println(sb);
    }

    static boolean stringPalindrom(String s){
        s = s.replaceAll("\\ ", "");
        StringBuilder st = new StringBuilder(s);
        String z = st.reverse().toString();
        return s.equalsIgnoreCase(z);
    }

    static void stringLength(String s) {
        if (s.length() > 10) {
            System.out.println(s.substring(0, 6));
        } else {
            StringBuilder sb = new StringBuilder(s);
            for (int i = sb.length(); i < 12; i++) {
                sb.append('o');
            }
            System.out.println(sb);
        }
    }

    static String replaceWord(String s) {
        Pattern pt = Pattern.compile("(\\W*)(\\w+)(.+?)(\\w+)(\\W*\\Z)");
        Matcher mc = pt.matcher(s);
        String s1 = mc.replaceAll("$1$4$3$2$5");
        return s1;
    }

    static String replaceWordInSentence(String s) {
        StringBuilder sb = new StringBuilder();
        Pattern pt = Pattern.compile(".+?[.!?]+");
        Matcher mt = pt.matcher(s);
        while (mt.find()) {
            sb.append(replaceWord(mt.group()));
        }
        return sb.toString();
    }

    static void abcContains(String s) {
//        Pattern pattern = Pattern.compile("[abc]+");
//        Matcher matcher = pattern.matcher(s);
        System.out.println(s.matches("[abc]+"));
    }

    static void dateContains(String s) {
        Pattern pattern = Pattern.compile("\\b(0[1-9]|1[0-2]|2[0-9]|3[0-1])\\.(0[1-9]|1[0-2])\\.\\d{4}\\b");
        Matcher matcher = pattern.matcher(s);
        System.out.println(matcher.matches());
    }

    static void adressContains(String s) {
        System.out.println(s.matches("\\b\\d{5}\\b\\, [а-я]\\. [А-Я][а-я]+\\, [а-я]+\\. [А-Я][а-я]+\\, [а-я]\\. \\d{1,3}[А-Я]*[а-я]*\\, [а-я]{2}\\. \\d{1,3}\\b"));
    }

    static String[] containsPhoneNumber(String s) {
        Pattern pattern = Pattern.compile("\\+{1}\\d{3}\\(\\d{2}\\)\\d{3}-\\d{2}-\\d{2}");
        Matcher matcher = pattern.matcher(s);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        matcher.reset();
        String[] numbers = new String[count];
        for (int i = 0; i < numbers.length && matcher.find(); i++) {
            numbers[i] = matcher.group();
        }
        return numbers;
    }

    static String readFile(String path) {
        String text = null;
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(path))){
            while ((text = br.readLine()) != null) {
                sb.append(text).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    static void markDown(String path) {
        String s = readFile(path);
        Pattern pattern = Pattern.compile("(\\A#{1})([^#])(.+)(\\n)");
        Matcher matcher = pattern.matcher(s);
        s = matcher.replaceAll("<h1>$2$3</h1>$4");
        Pattern pattern1 = Pattern.compile("(\\A#{2})([^#])(.+)(\\n)");
        Matcher matcher1 = pattern1.matcher(s);
        s = matcher1.replaceAll("<h2>$2$3</h2>$4");
        Pattern pattern2 = Pattern.compile("(\\A#{3})([^#])(.+)(\\n)");
        Matcher matcher2 = pattern2.matcher(s);
        s = matcher2.replaceAll("<h3>$2$3</h3>$4");
        Pattern pattern3 = Pattern.compile("([^*])(\\*{1})([^*].+?[^*])(\\*{1})([^*])");
        Matcher matcher3 = pattern3.matcher(s);
        s = matcher3.replaceAll("$1<em>$3</em>$5");
        Pattern pattern4 = Pattern.compile("([^*])(\\*{2})([^*].+?[^*])(\\*{2})([^*])");
        Matcher matcher4 = pattern4.matcher(s);
        s = matcher4.replaceAll("$1<strong>$3</strong>$5");
        Pattern pattern5 = Pattern.compile("\\[(.+)]\\((.+)\\)");
        Matcher matcher5 = pattern5.matcher(s);
        s = matcher5.replaceAll("<a href='$2'>$1</a>");
        Pattern pattern6 = Pattern.compile("([\\n]+)(.+)");
        Matcher matcher6 = pattern6.matcher(s);
        s = matcher6.replaceAll("$1<p>$2</p>");
        System.out.println("<html>\n<body> \n" + s + "</html>\n</body>");
    }
}
