package ua.org.oa.tolstoya.homework7.TimeThread;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Alex on 24.12.2016.
 */
public class TimeThread {
    public static void timeThread() {
        Thread th = new Thread() {
            @Override
            public void run() {
                while (true) {
                    System.out.println(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME));
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        };
        th.start();
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        th.interrupt();

    }

    public static void timeThreadLambda() {
        Thread t = new Thread(() -> {
            while (true) {
                System.out.println(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME));
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    return;
                }
            }
        });
        t.start();
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        t.interrupt();
    }
}
