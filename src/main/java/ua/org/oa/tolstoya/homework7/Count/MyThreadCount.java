package ua.org.oa.tolstoya.homework7.Count;

/**
 * Created by Alex on 27.12.2016.
 */
public class MyThreadCount extends Thread {


    public void increase() {
        synchronized (App.c) {
            System.out.println(App.c.getCount1() + " " + App.c.getCount2() + " " + (App.c.getCount1() == App.c.getCount2()));
            App.c.incCount1();
            try {
                sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            App.c.incCount2();
        }
    }

    @Override
    public void run() {
        increase();
        increase();
        increase();
    }
}
