package ua.org.oa.tolstoya.homework7.DeadLock;

public class Phone {
    public synchronized void makeCall(Human fromHuman, Human toHuman){
        System.out.println("Call to " + toHuman);
        toHuman.getPhone().reciveCall(fromHuman);
    }

    public synchronized void reciveCall(Human human){
        System.out.println("Incoming call from " + human);
    }
}
