package ua.org.oa.tolstoya.homework7.Count;

/**
 * Created by Alex on 27.12.2016.
 */
public class App {
    static Count c = new Count(0, 0);
    public static void main(String[] args) {
        MyThreadCount t1 = new MyThreadCount();
        MyThreadCount t2 = new MyThreadCount();
        MyThreadCount t3 = new MyThreadCount();

        t1.start();
        t2.start();
        t3.start();
    }
}
