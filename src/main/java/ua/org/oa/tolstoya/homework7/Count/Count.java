package ua.org.oa.tolstoya.homework7.Count;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alex on 27.12.2016.
 */
@Getter
@Setter
public class Count {
    private int count1;
    private int count2;

    public Count(int count1, int count2) {
        setCount1(count1);
        setCount2(count2);
    }

    public void incCount1() {
        count1++;
    }

    public void incCount2() {
        count2++;
    }

}
