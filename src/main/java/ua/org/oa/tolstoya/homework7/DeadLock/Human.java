package ua.org.oa.tolstoya.homework7.DeadLock;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Human extends Thread{
    private String humanName;
    private Phone phone;
    private Human friend;

    public Human(String name, Phone phone) {
        setHumanName(name);
        setPhone(phone);
    }

    @Override
    public void run() {
        phone.makeCall(this, friend);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + humanName + '\'' +
                "} ";
    }
}
