package ua.org.oa.tolstoya.homework7.DeadLock;

public class App {
    public static void main(String[] args) {
        Human vasay = new Human("Alex", new Phone());
        Human petay = new Human("Natali", new Phone());

        vasay.setFriend(petay);
        petay.setFriend(vasay);

        vasay.start();
        try {
            Thread.sleep(0,1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        petay.start();

    }
}
