package ua.org.oa.tolstoya.homework1;

/**
 * Created by Alex on 12.11.2016.
 */
public class App {
    public static void main(String[] args) {
        Date date = new Date(1, 3, 2019);
        System.out.println(date.getDayOfWeek());
        System.out.println(date.getDaysOfYear());
        System.out.println(date.daysBetween(new Date(1, 2, 2007)));

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        Car car1 = new Car() {
            @Override
            public boolean equals(Object o) {
                return super.equals(o);
            }

            @Override
            public String toString() {
                return "Audi A5 " + super.toString();
            }
        };

        Car car2 = new Car() {
            @Override
            public boolean equals(Object o) {
                return super.equals(o);
            }

            @Override
            public String toString() {
                return "BMW  M5 " + super.toString();
            }
        };

        System.out.println(car1);
        System.out.println(car2);
    }
}
