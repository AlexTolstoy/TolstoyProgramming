package ua.org.oa.tolstoya.homework1;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.MonthDay;
import java.time.Year;

/**
 * Created by Alex on 12.11.2016.
 */
@Data
public class Date {
    private Day day;
    private Month month;
    private Year year;

    public Date(int day, int month, int year) {
        setDay(new Day(day));
        setMonth(new Month(month));
        setYear(new Year(year));
    }

    public void changeDate(int day, int month, int year) {
        this.day.setDay(day);
        this.month.setMonth(month);
        this.year.setYear(year);
    }

    public DayOfWeek getDayOfWeek() {
        return DayOfWeek.valueOf((day.getDay() + month.codOfMonth + year.codOfYear) % 7);
    }

    public int getDaysOfYear() {
        int day = this.day.getDay();
        for (int i = 1; i < month.getMonth() ; i++) {
            day += month.getDays(i, year.isLeap());
        }
        return day;
    }

    public int daysBetween(Date date) {
        int daysInYear = 0;
        int dayNewDate = date.getDaysOfYear();
        if (date.getYear().getYear() < getYear().getYear()) {
            for (int i = date.getYear().getYear(); i < getYear().getYear(); i++) {
                if (date.getYear().isLeap()) {
                    daysInYear += - new Year(i).getNumberOfDays();
                }else {
                    daysInYear += new Year(i).getNumberOfDays();
                }
            }
        }else {
            for (int i = getYear().getYear(); i < date.getYear().getYear(); i++) {
                if (getYear().isLeap()) {
                    daysInYear += - new Year(i).getNumberOfDays();
                }else {
                    daysInYear += new Year(i).getNumberOfDays();
                }
            }
        }
        return Math.abs(Math.abs(getDaysOfYear() - dayNewDate) + daysInYear);
    }


    static class Year {
        @Getter
        @Setter
        private int year;
        @Getter
        @Setter
        private boolean leap;
        @Getter
        @Setter
        private int codOfYear;
        @Getter
        @Setter
        private int numberOfDays;

        private int calculateCodOfYear(int year) {
            int lastTwoNumber = year % 100;
            return (6 + lastTwoNumber + lastTwoNumber / 4) % 7;
        }

        public Year(int year) {
            setYear(year);
            setLeap(java.time.Year.of(year).isLeap());
            setCodOfYear(calculateCodOfYear(year));
            setNumberOfDays(isLeap() ? 366 : 365);
        }
    }

    static class Month {
        @Getter
        @Setter
        private int month;
        @Getter
        @Setter
        private int codOfMonth;

        private int calculateCodOfMonth(int month) {
            switch (month) {
                case 4:
                case 7:
                    return 0;
                case 1:
                case 10:
                    return 1;
                case 5:
                    return 2;
                case 8:
                    return 3;
                case 2:
                case 3:
                case 11:
                    return 4;
                case 6:
                    return 5;
                case 9:
                case 12:
                    return 6;
            }
            return -1;
        }

        public Month(int month) {
            setMonth(month);
            setCodOfMonth(calculateCodOfMonth(month));
        }

        public int getDays(int monthNumber, boolean leapYear) {
            switch (monthNumber) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    return 31;
                case 2:
                    return leapYear ? 29 : 28;
                case 4:
                case 6:
                case 9:
                case 11:
                    return 30;
            }
            return -1;
        }
    }

    static class Day {
        @Getter
        @Setter
        private int day;

        public Day(int day) {
            setDay(day);
        }
    }

    enum DayOfWeek {
        MONDAY(2), TUESDAY(3), WENSDAY(4), THIRSDAY(5), FRIDAY(6), SATURDAY(0), SUNDAY(1);

        @Getter
        @Setter
        int index;

        DayOfWeek(int index) {
            setIndex(index);
        }

        public static DayOfWeek valueOf(int index) {
            for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
                if (dayOfWeek.getIndex() == index) {
                    return dayOfWeek;
                }
            }
            return null;
        }
    }


}
