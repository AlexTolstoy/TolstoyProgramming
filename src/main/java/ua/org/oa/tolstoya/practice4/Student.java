package ua.org.oa.tolstoya.practice4;

import lombok.Data;

/**
 * Created by Alex on 22.11.2016.
 */
@Data
public class Student implements Comparable<Student>{
    private String firstName;
    private String lastName;
    private int course;


    public Student(String firstName, String lastName, int course) {
        setFirstName(firstName);
        setLastName(lastName);
        setCourse(course);
    }

    @Override
    public int compareTo(Student o) {
        return getFirstName().compareTo(o.getFirstName());
    }
}
