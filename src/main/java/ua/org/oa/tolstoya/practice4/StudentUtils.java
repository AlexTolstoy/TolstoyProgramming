package ua.org.oa.tolstoya.practice4;

import ua.org.oa.tolstoya.practice2.Parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Alex on 22.11.2016.
 */
public class StudentUtils {
    public static void main(String[] args) throws IOException {
        List<Student> students = new ArrayList<>(Arrays.asList(
                new Student("Алексей", "Толстой", 1), new Student("Святослав", "Котляр", 1),
                new Student("Павел", "Чуваков", 1), new Student("Оксана", "Заика", 2), new Student("Наталия", "Верченко", 2)));
        StudentUtils.createMapFromList(students);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~");
        StudentUtils.printStudents(students, 1);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~");
        StudentUtils.sortStudents(students);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~");
        StudentUtils.repeatWord();
    }
    static Map<String, Student> createMapFromList(List<Student> students) {
        Map<String, Student> studentMap = new HashMap<>();
        for (Student student : students) {
            studentMap.put(student.getFirstName().concat(" ").concat(student.getLastName()), student);
        }
        for (Map.Entry<String, Student> studentEntry : studentMap.entrySet()) {
            System.out.println(studentEntry.getKey() + " -> " + studentEntry.getValue());
        }
            return studentMap;
    }

    static void printStudents(List<Student> students, int course) {
        Student student1;
        for (Iterator<Student> iterator = students.iterator(); iterator.hasNext();) {
            student1 = iterator.next();
            if (student1.getCourse() == course) {
                System.out.println(student1);
            }
        }
    }

    static List<Student> sortStudents(List<Student> students) {
        List<Student> students1 = students;
        Collections.sort(students);
        students1.forEach(System.out::println);
        return students1;
    }

    static void repeatWord() throws IOException {
//        String s = Parser.parseString("Romeo.txt", "utf-8");
//        System.out.println(s);
        BufferedReader bf = new BufferedReader(new FileReader("Romeo.txt"));
        StringBuilder contents = new StringBuilder();
        String text = null;
        while ((text = bf.readLine()) != null) {
            contents.append(text).append("\n");
        }
        String s = contents.toString();
        String[] words = s.split("[ ,.!?]+");
    }
}
