package ua.org.oa.tolstoya.practice5;

import java.io.*;
import java.util.*;


/**
 * Created by Alex on 29.11.2016.
 */
public class StreamUtils {
    public static int[] randomNumbers() {
        int[] ints = new int[10];
        Random random = new Random();
        for (int i = 0; i < ints.length; i++) {
            ints[i] = random.nextInt(50);
        }
        //System.out.println(Arrays.toString(ints));
        return ints;

    }

    public static void writeToFile(String text, String path) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path))) {
            bw.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createFile(String path) {
        StringBuilder sb = new StringBuilder();
        for (int numbers : randomNumbers()) {
            sb.append(numbers).append("\n");
        }
        writeToFile(sb.toString(), path);
    }

    public static void sort(String path) {
        List<Integer> integers = new ArrayList<>();
        try (Scanner scanner = new Scanner(new FileInputStream(path))) {
            while (scanner.hasNext()) {
                integers.add(Integer.parseInt(scanner.next()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Collections.sort(integers);
        integers.forEach(System.out::println);

    }
}
