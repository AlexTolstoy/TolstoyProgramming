package ua.org.oa.tolstoya.homework5;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Alex on 17.12.2016.
 */
public class Dictionary {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public String readConsole() {
        String s = null;
        try {
            s = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    public String readFile(File path) {
        String s = null;
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "windows-1251"))){
            while ((s = br.readLine()) != null) {
                sb.append(s).append("\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
    public void console() {
        System.out.println("Словарь");
        System.out.println("Перевести текст = Y\nВыйти = N");
        String s = readConsole();
        switch (s.toLowerCase()) {
            case"y":
                System.out.println("Введите путь к файлу");
                translate(readFile(new File(readConsole())));
        }
    }

    public void translate(String text) {
        System.out.println("Выберите направление перевода");
        File[] files = new File(Dictionary.class.getClassLoader().getResource("dictionary").getPath()).listFiles();
        for (int i = 0; i < files.length; i++) {
            System.out.println(i + " " + files[i].getName());
        }
        String dict = readFile(files[Integer.parseInt(readConsole())]);
        Map<String, String> trans = new HashMap<>();
        Pattern pattern = Pattern.compile("(.+);-;(.+)");
        Matcher matcher = pattern.matcher(dict);
        while (matcher.find()) {
            trans.put(matcher.group(1), matcher.group(2));
        }
        StringBuilder sb = new StringBuilder();
        Pattern pattern1 = Pattern.compile("([а-яa-zА-ЯA-Z]+)([ .,!?]*)");
        Matcher matcher1 = pattern1.matcher(text);
        while (matcher1.find()) {
            String s = matcher1.group(1);
            boolean b = s.matches("[А-ЯA-Z].*");
            if(b)
            s=s.toLowerCase();
            String t;
                if ((t = trans.get(s ))!=null) {
                    if(b){
                        t = String.valueOf(t.charAt(0)).toUpperCase() + t.substring(1);
                    }
                    sb.append(t);
                }else{
                    if(b){
                        s = String.valueOf(s.charAt(0)).toUpperCase() + s.substring(1);
                    }
                    sb.append(s);
                }
                sb.append(matcher1.group(2));
            }
        System.out.println(sb.toString());
    }
}
