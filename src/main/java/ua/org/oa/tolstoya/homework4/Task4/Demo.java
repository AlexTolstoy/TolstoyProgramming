package ua.org.oa.tolstoya.homework4.Task4;

import java.util.Iterator;

/**
 * Created by Alex on 10.12.2016.
 */
public class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addFirst(433);
        deque.addFirst(545);
        deque.addFirst(447);
        deque.addFirst(78876);
        deque.addLast(8.88);
        deque.addLast(3.56);
        deque.addLast(1.11);

        ListIterator<Number> listIt = deque.listIterator();
        while (listIt.hasNext())
            System.out.println(listIt.next());
        System.out.println("~~~~~~~~~~~~~~~~~~~");
        while (listIt.hasPrevious())
            System.out.println(listIt.previous());

    }
}