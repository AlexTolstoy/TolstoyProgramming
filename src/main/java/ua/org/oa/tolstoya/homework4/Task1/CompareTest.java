package ua.org.oa.tolstoya.homework4.Task1;

/**
 * Created by Alex on 10.12.2016.
 */
public class CompareTest {
    public static void main(String[] args) {
        Car[] cars = new Car[3];
        Computer[] computers = new Computer[3];
        Integer[] ints = new Integer[3];
        String[] st = new String[3];

        cars[0] = new Car("BMW", 250);
        cars[1] = new Car("Audi", 270);
        cars[2] = new Car("Mercedes", 240);

        computers[0] = new Computer("Asus", 500);
        computers[1] = new Computer("Dell", 800);
        computers[2] = new Computer("Apple", 1800);

        ints[0] = 5;
        ints[1] = 2;
        ints[2] = 1;

        st[0] = "Hello";
        st[1] = "Hi";
        st[2] = "Yo";

        //CompareUtils.compareArray(cars); //ошибка компиляции
        System.out.println(CompareUtils.compareArray(computers));
        System.out.println(CompareUtils.compareArray(ints));
        System.out.println(CompareUtils.compareArray(st));
    }
}
