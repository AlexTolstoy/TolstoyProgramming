package ua.org.oa.tolstoya.homework4.Task4;

/**
 * Created by Alex on 17.12.2016.
 */
public interface ListIterable<E> {
    ListIterator<E> listIterator();
}
