package ua.org.oa.tolstoya.homework4.Task1;

import lombok.Data;

/**
 * Created by Alex on 10.12.2016.
 */
@Data
public class Computer implements Comparable<Computer>{
    private String model;
    private double price;

    public Computer(String model, double price) {
        this.model = model;
        this.price = price;
    }

    @Override
    public int compareTo(Computer o) {
        return Double.compare(price, o.getPrice());
    }
}
