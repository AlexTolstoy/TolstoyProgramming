package ua.org.oa.tolstoya.homework4.Task3;

import java.util.Iterator;

/**
 * Created by Alex on 10.12.2016.
 */
public class Demo {
    public static void main(String[] args) {
        MyDeque<Number> deque = new MyDequeImpl<>();
        deque.addFirst(433);
        deque.addFirst(545);
        deque.addFirst(447);
        deque.addFirst(78876);
        deque.addLast(8.88);
        deque.addLast(3.56);
        deque.addLast(1.11);

        for (Number element : deque) {
            System.out.println(element);
        }
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Iterator<Number> it = deque.iterator();

        while (it.hasNext())

            System.out.println(it.next());

    }
}