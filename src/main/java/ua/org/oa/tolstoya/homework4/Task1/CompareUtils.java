package ua.org.oa.tolstoya.homework4.Task1;

/**
 * Created by Alex on 10.12.2016.
 */
public class CompareUtils {
    public static Comparable compareArray(Comparable[] array) {
        Comparable max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (max.compareTo(array[i]) < 0) {
                max = array[i];
            }
        }
        return max;
    }
}
