package ua.org.oa.tolstoya.homework4.Task1;

import lombok.Data;

/**
 * Created by Alex on 10.12.2016.
 */
@Data
public class Car {
    private String brand;
    private int speed;

    public Car(String brand, int speed) {
        this.brand = brand;
        this.speed = speed;
    }
}
