package ua.org.oa.tolstoya.homework4.Task2;

/**
 * Created by Alex on 10.12.2016.
 */
public class MyDequeImpl<E> implements MyDeque<E> {
    private Node<E> first;
    private Node<E> last;
    private int size;

    @Override
    public void addFirst(E e) {
        Node<E> f = first;
        Node<E> e1 = new Node<>(null, e, first);
        first = e1;
        size++;
        if (f == null) {
            last = e1;
        }else {
            f.prev = e1;
        }
    }

    @Override
    public void addLast(E e) {
        Node<E> l = last;
        Node<E> e1 = new Node<>(last, e, null);
        last = e1;
        size++;
        if (l == null) {
            first = e1;
        }else {
            l.next = e1;
        }
    }

    @Override
    public E removeFirst() {
        Node<E> remove = first;
        if (first == null) {
            return null;
        }else if (first.next != null) {
            first = first.next;
        }else {
            last = null;
        }
        size--;
        return remove.element;
    }

    @Override
    public E removeLast() {
        Node<E> remove = last;
        if (last == null) {
            return null;
        }else if (last.prev != null) {
            last = last.prev;
        }else {
            first = null;
        }
        size--;
        return remove.element;
    }

    @Override
    public E getFirst() {
        return first.element;
    }

    @Override
    public E getLast() {
        return last.element;
    }

    @Override
    public boolean contains(Object o) {
        for (Node<E> i = first; i != null; i = i.next) {
            if (i.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public Object[] toArray() {
        int count = 0;
        Object[] array = new Object[size];
        for (Node<E> i = first; i != null; i = i.next) {
            array[count++] = i.element;
        }
        return array;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean containsAll(MyDeque<? extends E> deque) {
        for (Object o : deque.toArray()) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    private static class Node<E> {

// хранимый элемент

        E element;

// ссылка на следующий элемент списка

        Node<E> next;

// ссылка на предыдущий элемент списка

        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.prev = prev;
            this.element = element;
            this.next = next;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Node<E> i = first; i != null; i = i.next) {
            sb.append(i.element).append(", ");
        }
        return sb.toString();
    }
}
