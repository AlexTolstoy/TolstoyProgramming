package ua.org.oa.tolstoya.practice6.MyShedule;

import java.util.Map;

/**
 * Created by Alex on 06.12.2016.
 */

public class MyShedule {

    public void sheduleRun(Map<Integer, String> map) {
        for (Map.Entry<Integer, String> mapEntry : map.entrySet()) {
            System.out.println(mapEntry.getValue());
            try {
                Thread.sleep(mapEntry.getKey());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
