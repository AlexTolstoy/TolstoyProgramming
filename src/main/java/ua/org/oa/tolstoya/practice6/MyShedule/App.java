package ua.org.oa.tolstoya.practice6.MyShedule;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alex on 06.12.2016.
 */
public class App {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(2, "Second");
        map.put(3, "Third");
        map.put(1, "First");

        MyShedule shedule = new MyShedule();
        shedule.sheduleRun(map);
    }
}
