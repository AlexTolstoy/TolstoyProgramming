package ua.org.oa.tolstoya.practice6.Thread;

/**
 * Created by Alex on 06.12.2016.
 */
public class RannableThread implements Runnable {
    @Override
    public void run() {
        Thread thread = Thread.currentThread();
        for (int i = 0; i < 10; i++) {
            System.out.println(thread.getName());
            try {
                thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
