package ua.org.oa.tolstoya.practice6.Thread;

/**
 * Created by Alex on 06.12.2016.
 */
public class MyThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10 ; i++) {
            System.out.println(getName());
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
