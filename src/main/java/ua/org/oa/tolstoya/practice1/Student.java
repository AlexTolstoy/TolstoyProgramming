package ua.org.oa.tolstoya.practice1;

import java.util.ArrayList;

public class Student {
    private  String name;
    private  String surname;
    private  String groupId;
    ArrayList<Exam> examList = new ArrayList<>();

    static class Exam{
        private String name;
        private int mark;
        private int year;
        private int semestr;

        public Exam(String name, int mark, int year, int semestr) {
            this.name = name;
            this.mark = mark;
            this.year = year;
            this.semestr = semestr;

        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getMark() {
            return mark;
        }

        public void setMark(int mark) {
            this.mark = mark;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public int getSemestr() {
            return semestr;
        }

        public void setSemestr(int semestr) {
            this.semestr = semestr;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Exam)) return false;

            Exam exam = (Exam) o;

            if (mark != exam.mark) return false;
            if (year != exam.year) return false;
            if (semestr != exam.semestr) return false;
            return name != null ? name.equals(exam.name) : exam.name == null;

        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + mark;
            result = 31 * result + year;
            result = 31 * result + semestr;
            return result;
        }

        @Override
        public String toString() {
            return "Exam{" +
                    "name='" + name + '\'' +
                    ", mark=" + mark +
                    ", year=" + year +
                    ", semestr=" + semestr +
                    '}';
        }
    }

    public Student(String name, String surname, String groupId) {
        this.name = name;
        this.surname = surname;
        this.groupId = groupId;
    }

    public void addMark(String name, int mark, int year, int semestr) {
        this.examList.add(new Exam(name,mark,year,semestr));
    }

    public void deleteMark(String name, int mark, int year, int semestr) throws IllegalArgumentException {
        Exam ex = new Exam(name, mark, year, semestr);
        if (this.getExamList().contains(ex)) {
        this.getExamList().remove(new Exam(name,mark,year,semestr));}
        else {
        throw new IllegalArgumentException ("No such mark in the examlist!");
        }

    }

    public int maxMark(String name) throws IllegalArgumentException {
        int mark=0;
        boolean fl = true;
        if (this.getExamList().size()==0) {
        throw new IllegalArgumentException("No such mark in the examlist!");
        }

        for(int i = 0; i < this.getExamList().size(); i++ ) {
            if (name.equals(this.getExamList().get(i).getName())) {
                if (this.getExamList().get(i).getMark() > mark) {
                    mark = this.getExamList().get(i).getMark();
                    fl = false;
                }
            }
        }

        if (fl) {throw new IllegalArgumentException("No such mark in the examlist!");}
        else
        return mark;
    }

    public int numberOfExamsWithMark(int n) {

        if (this.getExamList().size()==0) {
            throw new IllegalArgumentException("No such mark in the examlist!");
        }

        int counter=0;

        for(int i = 0; i < this.getExamList().size(); i++ )
        {
            if (n == this.getExamList().get(i).getMark())
            {
                    counter++;
            }
        }
        return counter;
    }

    public double avarageMarkAtSemester(int s) {
        if (this.getExamList().size()==0) {
            throw new IllegalArgumentException("No such mark in the examlist!");
        }
        int sum = 0;
        int counter=0;

        for(int i = 0; i < this.getExamList().size(); i++ )
        {
            if (s == this.getExamList().get(i).getSemestr())
            {
                sum+= this.getExamList().get(i).getMark();
                counter++;
            }
        }

        if (counter == 0) {throw new IllegalArgumentException("No such mark in the examlist!");}
        else

                return Math.ceil(sum/counter*100)/100;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public ArrayList<Exam> getExamList() {
        return examList;
    }

}
