package ua.org.oa.tolstoya.practice1;

public class App {

    public static void main(String[] args) {

        int[] array = {1, 2, 3, 4, 5, 6, 7, 8};
        System.out.println(ArraySum.sumStatic(array));
        ArraySum a = new ArraySum(array);
        System.out.println(a.sum());
    }

}
