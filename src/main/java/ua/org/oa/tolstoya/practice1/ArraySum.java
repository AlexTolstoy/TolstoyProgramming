package ua.org.oa.tolstoya.practice1;

/**
 * Created by Егор on 01.11.2016.
 */
public class ArraySum {
    int[] array;

    public ArraySum(int[] array) {
        this.array = array;
    }

    public static int sumStatic(int[] arr) {
               int sum = 0;
                for (int i=0; i < arr.length; i++) {
                    sum+= arr[i];
                }
                return sum;
            } //статический метод суммирования массива

    public int sum() {
        int sum = 0;
        for (int i=0; i < this.getArray().length; i++) {
            sum+= this.getArray()[i];
        }
        return sum;

    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }
}
