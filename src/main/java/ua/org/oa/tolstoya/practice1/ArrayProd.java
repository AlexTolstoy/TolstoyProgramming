package ua.org.oa.tolstoya.practice1;

public class ArrayProd {
    public static long prod(int[] array) {
        long prod = 1;
        for(int i =0; i < array.length;i++ ) {
            prod = prod * array[i];
        }
        return prod;
    }

}
