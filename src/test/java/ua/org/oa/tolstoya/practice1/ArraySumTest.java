package ua.org.oa.tolstoya.practice1;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ArraySumTest {

    private static int[] testArray;

    @BeforeClass
    public static void setUpTestArray() {
        int n = (int) Math.ceil(Math.random()*10);
        testArray = new int[n];
        for(int i=0; i < n; i++) {
            testArray[i] = (int) Math.ceil(Math.random()*10);
        }

    }

    @Test
    public void sumTest(){

        int testSum = 0;
        for(int i=0; i < testArray.length; i++) {
            testSum+= testArray[i];
        }
        Assert.assertEquals(testSum, ArraySum.sumStatic(testArray));
    }

    @Test
    public void sumTestExempl() {
        ArraySum a = new ArraySum(testArray);
        int testSum = 0;
        for(int i=0; i < testArray.length; i++) {
            testSum+= testArray[i];
        }
        Assert.assertEquals(testSum, a.sum());
    }

    @Test(expected = NullPointerException.class)
    public void sumTestNull() {
        ArraySum.sumStatic(null);
    }
}
