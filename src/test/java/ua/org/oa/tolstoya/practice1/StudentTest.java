package ua.org.oa.tolstoya.practice1;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class StudentTest {

    private static Student st;

    @BeforeClass
    public static void setUpStudent() {
        st = new Student("Alex","Tolstoy","PTOCTXVI");
        st.addMark("Geography", 5, 2016, 1);
        st.addMark("Math", 3, 2016, 1);
        st.addMark("Science", 4, 2016, 1);
        st.addMark("PE", 3, 2016, 1);
        st.addMark("Literature", 5, 2016, 1);
        st.addMark("Math", 4, 2016, 1);
    }

    @Test
    public void testAddMark() {
        st.addMark("TestMark",1,1,1);

        Assert.assertEquals("TestMark", st.getExamList().get(st.getExamList().size()-1).getName());
        Assert.assertEquals(1 , st.getExamList().get(st.getExamList().size()-1).getMark());
        Assert.assertEquals(1, st.getExamList().get(st.getExamList().size()-1).getYear());
        Assert.assertEquals(1, st.getExamList().get(st.getExamList().size()-1).getSemestr());
        st.getExamList().remove(st.getExamList().size()-1);
    }

    @Test
    public void testDeleteMark() {
        st.addMark("TestMark",1,1,1);
        st.deleteMark("TestMark",1,1,1);
        Assert.assertFalse(st.getExamList().contains(new Student.Exam("TestMark",1,1,1)));

    }

    @Test
    public void testMaxMark() {
        Assert.assertEquals(st.maxMark("Math"), 4);
    }

    @Test
    public void testNumberOfExamsWithMark(){
        Assert.assertEquals(st.numberOfExamsWithMark(5), 2);
    }

    @Test
    public void testAvarageMarkAtSemestr(){
        Assert.assertEquals(st.avarageMarkAtSemester(1), 4, 0.1);
    }
}
