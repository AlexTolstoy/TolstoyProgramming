package ua.org.oa.tolstoya.practice1;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ArrayProdTest {

    private static int[] testArray;

    @BeforeClass
    public static void setUpTestArray() {
        int n = (int) Math.ceil(Math.random()*10);
        testArray = new int[n];
        for(int i=0; i < n; i++) {
            testArray[i] = (int) Math.ceil(Math.random()*10);
        }

    }

    @Test
    public void testProd() {
        long prod = 1;
        for(int i =0; i < testArray.length;i++ ) {
            prod = prod * testArray[i];
        }

        Assert.assertEquals(prod, ArrayProd.prod(testArray));
    }
}
