package ua.org.oa.tolstoya.practice1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ArrayProdTest.class, ArraySumTest.class, StudentTest.class})

public class AllTests {
}
